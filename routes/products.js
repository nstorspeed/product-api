const express = require('express');
const router = express.Router();
const Products = require('../models/Products');

let code = 0;
router.get('/', async(req, res) => {
    try {
        const product = await Products.find();
        code = product.length > 0 ? 200 : 204;
        responseJson = {
            "error": null,
            "code_response": code,
            "data": product
        }
        res.status(code).json(responseJson);
    } catch (err) {
        responseJson = {
            "error": err.message,
            "code_response": err.code,
            "data": null
        }
        res.status(400).json(responseJson);
    }
});

router.post('/', async(req, res) => {
    validate = validateData(req);
    if (Array.isArray(validate) && validate.length > 0) {
        responseJson = {
            "error": validate,
            "code_response": 400,
            "data": null
        }
        res.status(400).json(responseJson);
    } else {
        const product = new Products({
            name_product: req.body.name_product,
            code: req.body.code,
            value: req.body.value
        });

        try {
            const products = await product.save();
            responseJson = {
                "error": null,
                "code_response": 201,
                "data": products
            }
            res.status(201).json(responseJson);
        } catch (err) {
            responseJson = {
                "error": err.message,
                "code_response": err.code,
                "data": null
            }
            res.status(400).json(responseJson);
        }
    }

});

router.get('/:productId', async(req, res) => {

    if (!req.body.name_product) {
        responseJson = {
            "error": "You must send the _id data.",
            "code_response": 400,
            "data": null
        }
        res.status(400).json(responseJson);
    } else {
        try {
            const product = await Products.findById(req.params.productId);
            code = product == null ? 404 : 200;
            responseJson = {
                "error": null,
                "code_response": code,
                "data": product
            }
            res.status(code).json(responseJson);
        } catch (err) {
            responseJson = {
                "error": err.message,
                "code_response": 400,
                "data": null
            }
            res.status(400).json(responseJson);
        }
    }

});

router.delete('/:productId', async(req, res) => {
    if (!req.body.name_product) {
        responseJson = {
            "error": "You must send the _id data.",
            "code_response": 400,
            "data": null
        }
        res.status(400).json(responseJson);
    } else {
        try {
            const product = await Products.remove({ _id: req.params.productId });
            responseJson = {
                "error": null,
                "code_response": 201,
                "data": product
            }
            res.status(201).json(responseJson);
        } catch (err) {
            responseJson = {
                "error": err.message,
                "code_response": err.code,
                "data": null
            }
            res.status(400).json(responseJson);
        }
    }
});

router.patch('/:productId', async(req, res) => {
    validate = validateData(req);
    if (Array.isArray(validate) && validate.length > 0) {
        responseJson = {
            "error": validate,
            "code_response": 400,
            "data": null
        }
        res.status(400).json(responseJson);
    } else {
        try {
            const product = await Products.updateOne({ _id: req.params.productId }, {
                $set: {
                    name_product: req.body.name_product,
                    code: req.body.code,
                    value: req.body.value
                }
            });
            responseJson = {
                "error": null,
                "code_response": 201,
                "data": product
            }
            res.status(201).json(responseJson);
        } catch (err) {
            responseJson = {
                "error": err.message,
                "code_response": err.code,
                "data": null
            }
            res.status(400).json(responseJson);
        }
    }
});

function validateData(req) {
    let errorArray = [];
    if (!req.body.name_product) {
        errorArray.push("You must need insert the product name.");
    }
    if (!req.body.code) {
        errorArray.push("You must need insert the product code.");
    }
    if (!req.body.value) {
        errorArray.push("You must need insert the product value.");
    }
    return errorArray;
}

module.exports = router;