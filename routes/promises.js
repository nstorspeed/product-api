const express = require('express');
const router = express.Router();
const Products = require('../models/Products');


let code = 0;
router.get('/', (req, res) => {
    console.log("hola");
    Products.find()
        .then(data => {
            code = data.length > 0 ? 200 : 204;
            responseJson = {
                "error": null,
                "code_response": code,
                "data": data
            }
            res.status(code).json(responseJson);
        })
        .catch(err => {
            responseJson = {
                "error": err.message,
                "code_response": 400,
                "data": null
            }
            res.status(400).json(responseJson);
        });
});

router.post('/', (req, res) => {
    validate = validateData(req);
    if (Array.isArray(validate) && validate.length > 0) {
        responseJson = {
            "error": validate,
            "code_response": 400,
            "data": null
        }
        res.status(400).json(responseJson);
    } else {
        const product = new Products({
            name_product: req.body.name_product,
            code: req.body.code,
            value: req.body.value
        });

        product.save()
            .then(data => {
                responseJson = {
                    "error": null,
                    "code_response": 201,
                    "data": data
                }
                res.status(201).json(responseJson);
            })
            .catch(error => {
                responseJson = {
                    "error": error.message,
                    "code_response": 400,
                    "data": null
                }
                res.status(400).json(responseJson);
            });
    }

});

router.get('/:productId', (req, res) => {

    if (!req.body.name_product) {
        responseJson = {
            "error": "You must send the _id data.",
            "code_response": 400,
            "data": null
        }
        res.status(400).json(responseJson);
    } else {
        Products.findById(req.params.productId)
            .then(data => {
                responseJson = {
                    "error": null,
                    "code_response": code,
                    "data": data
                }
                res.status(code).json(responseJson);
            })
            .catch(error => {
                responseJson = {
                    "error": error.message,
                    "code_response": 400,
                    "data": null
                }
                res.status(400).json(responseJson);
            });

    }

});

router.delete('/:productId', (req, res) => {
    console.log(req.body);
    if (!req.body._id) {
        responseJson = {
            "error": "You must send the _id data.",
            "code_response": 400,
            "data": null
        }
        res.status(400).json(responseJson);
    } else {
        Products.deleteOne({ _id: req.params.productId })
            .then(data => {
                responseJson = {
                    "error": null,
                    "code_response": 201,
                    "data": data
                }
                res.status(201).json(responseJson);
            })
            .catch(error => {
                responseJson = {
                    "error": error.message,
                    "code_response": 400,
                    "data": null
                }
                res.status(400).json(responseJson);
            });

    }
});

router.patch('/:productId', (req, res) => {
    validate = validateData(req);
    if (Array.isArray(validate) && validate.length > 0) {
        responseJson = {
            "error": validate,
            "code_response": 400,
            "data": null
        }
        res.status(400).json(responseJson);
    } else {
        Products.updateOne({ _id: req.params.productId }, {
                $set: {
                    name_product: req.body.name_product,
                    code: req.body.code,
                    value: req.body.value
                }
            })
            .then(data => {
                responseJson = {
                    "error": null,
                    "code_response": 201,
                    "data": data
                }
                res.status(201).json(responseJson);
            })
            .catch(error => {
                responseJson = {
                    "error": error.message,
                    "code_response": 400,
                    "data": null
                }
                res.status(400).json(responseJson);
            });
    }
});

function validateData(req) {
    let errorArray = [];
    if (!req.body.name_product) {
        errorArray.push("You must need insert the product name.");
    }
    if (!req.body.code) {
        errorArray.push("You must need insert the product code.");
    }
    if (!req.body.value) {
        errorArray.push("You must need insert the product value.");
    }
    return errorArray;
}

module.exports = router;