const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv/config');

//Ports
app.set('port', process.env.PORT || 5000);
app.set('json spaces', 2);

//Import Routes
const productRoute = require('./routes/products');

//Middleware
app.use(cors());
app.use(bodyParser.json());
app.use('/products', productRoute);
app.use('/promises', productRoute);

// Creating routes
app.get('/', (req, res) => {
    res.json({ "response": "Hello from Home" });
});


// Connecting MongoDB
mongoose.connect(process.env.DB_CONNECTION, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
    })
    .then(() => {
        console.log('DB Connected!')
    })
    .catch(err => {
        console.log("An error has occurred: " + err.message);
    });
// Start server
app.listen(app.get('port'), () => {
    console.log(`Server online in port: ${app.get('port')}`);
});