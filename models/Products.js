const mongoose = require('mongoose');

//Creating schemas
const ProductsSchema = mongoose.Schema({
    name_product: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    value: {
        type: String,
        required: true,
        default: "0"
    },

});

module.exports = mongoose.model('Products', ProductsSchema);